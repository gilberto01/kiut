<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'titulo' => 'Conoce el significado de tus sueños',
            'descripcion' => 'No dejes que tus sueños pasen desapercibidos. Practica escribir todos tus sueños para conocerte más a ti misma. ¡Usa tu kiut especial de los sueños!',
            'imagen' => 'img_noticia_3.jpg',
            'categoria_id' => 1,
            'user_id' => 1,
        ]);
        DB::table('posts')->insert([
            'titulo' => 'Organiza tu agenda. Tips para planear tus actividades',
            'descripcion' => 'Con nuestras agendas y planeadores kiut será más fácil organizar tu tiempo y actividades, sin perder el estilo.',
            'imagen' => 'img_noticia_2.jpg',
            'categoria_id' => 1,
            'user_id' => 1,
        ]);
        DB::table('posts')->insert([
            'titulo' => 'Los 10 mejores looks para este regreso a clases',
            'descripcion' => '¡No pases desapercibida! Define tu estilo y regresa radiante con estos increíbles outfits para ti.',
            'imagen' => 'img_noticia_1.jpg',
            'categoria_id' => 1,
            'user_id' => 1,
        ]);
        DB::table('posts')->insert([
            'titulo' => 'Qué color te define',
            'descripcion' => '¿Qué dice de ti tu color favorito? Con nuestros colores y marcadores kiut podrás elegir entre metalizados, pasteles, neón, tropicales… y descubrir cuál es tu color.',
            'imagen' => '',
            'categoria_id' => 1,
            'user_id' => 1,
        ]);
    }
}
