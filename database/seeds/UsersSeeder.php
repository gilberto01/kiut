<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Gilberto Corrales Ibarra',
            'email' => 'gilberto.corrales01@gmail.com',
            'password' => Hash::make('123'),
        ]);
    }
}
