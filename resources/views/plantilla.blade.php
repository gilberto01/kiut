<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Kiut</title>

    <link rel="stylesheet" href="{{asset('css/uikit.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/estilo.css')}}" />
    <script src="{{asset('js/uikit.min.js')}}"></script>
    <script src="{{asset('js/uikit-icons.min.js')}}"></script>
    <script src="https://use.fontawesome.com/releases/v5.14.0/js/all.js" data-auto-replace-svg="nest"></script>
    <script
            src="https://code.jquery.com/jquery-3.5.1.js"
            integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="
            crossorigin="anonymous"></script>
</head>
<body>

<div id="preloader" class="uk-flex-middle">
    <div id="status" class="uk-margin-medium-top uk-text-center">
        <div class="uk-flex-middle" uk-grid>
            <div class="uk-width-1-1@m uk-flex-first">
                <img src="{{asset('img/carga.gif')}}">
            </div>
        </div>
    </div>
</div>

<div class="FondoMenu">
    <div class="BarraMenu Menu1">
        <div class="uk-container">
            <div class="uk-grid uk-flex uk-flex-right">
                <div class="uk-width-1-3@m">
                    <div class="uk-grid">
                        <div class="uk-width-expand EnlaceRedes" uk-scrollspy="target: > a; cls: uk-animation-slide-top; delay: 100; repeat: true">
                            <a href="" class="uk-icon-button uk-margin-small-right" uk-icon="icon: instagram; ratio: .8"></a>
                            <a href="" class="uk-icon-button  uk-margin-small-right" uk-icon="icon: facebook; ratio: .8"></a>
                            <a href="" class="uk-icon-button" uk-icon="icon: youtube; ratio: .8"></a>
                        </div>
                        <div class="uk-width-auto EnlaceRedes">
                            <p> CONTACTO (55) 26265500</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--MENU RESPONSIVO-->
    <div class="Menu2">
        <nav class="uk-navbar uk-navbar-container">
            <div class="uk-navbar-left">
                <a class="uk-navbar-toggle" href="#" uk-toggle="target: #offcanvas-nav-primary">
                    <span uk-navbar-toggle-icon></span> <span class="uk-margin-small-left">Menu</span>
                </a>
            </div>
            <div class="uk-navbar-right">
                <a class="uk-navbar-item uk-logo" href="#"><img src="{{asset('img/img_menu_logo.png')}}" class="ImgLogoMenu" uk-scrollspy="cls: uk-animation-slide-top; delay: 150; repeat: true"></a>
            </div>
        </nav>
    </div>

    <div id="offcanvas-nav-primary" uk-offcanvas="overlay: true">
        <div class="uk-offcanvas-bar uk-flex uk-flex-column">

            <ul class="uk-nav uk-nav-primary uk-nav-center uk-margin-auto-vertical">
                <li class="uk-nav-header">Menu</li>
                <li class="uk-active"><a href="#">Inspirate con kiut</a></li>
                <li><a href="#">Catálago</a></li>
                <li><a href="#">Descargable</a></li>
                <li><a href="#">Dónde comprar</a></li>
            </ul>

        </div>
    </div>

    <div class="uk-position-relative uk-container">
        <img src="{{asset('img/banner.jpg')}}" class="ImgBanner">
        <div class="uk-position-top">
            <nav class="uk-navbar-container Menu1 uk-navbar-transparent" uk-navbar>
                <div class="uk-navbar-center" uk-scrollspy="target: > div, ul, li; cls: uk-animation-fade; delay: 20; repeat: true">
                    <div class="uk-navbar-center-left">
                        <ul class="uk-navbar-nav">
                            <li class="uk-active"><a href="#">inspirate con kiut</a></li>
                            <li><a href="#">catálago</a></li>
                        </ul>
                    </div>
                    <a class="uk-navbar-item uk-logo" href="#"><img src="{{asset('img/img_menu_logo.png')}}" class="ImgLogoMenu" uk-scrollspy="cls: uk-animation-slide-top; delay: 150; repeat: true"></a>
                    <div class="uk-navbar-center-right">
                        <ul class="uk-navbar-nav">
                            <li><a href="#">descargable</a></li>
                            <li><a href="#">dónde comprar</a></li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div class="uk-container ContenidoBanner">
                <div class="uk-grid">
                    <div class="uk-width-1-2@m uk-text-right ImgBannerCenter">
                        <img src="img/img_1.png" uk-scrollspy="cls: uk-animation-slide-bottom; delay: 300; repeat: true">
                        <div class="uk-text-right BtnBanner"  uk-scrollspy="cls: uk-animation-slide-bottom; delay: 500; repeat: true">
                            <a>conoce más</a>
                        </div>
                    </div>
                    <div class="uk-width-1-2"></div>
                </div>
            </div>
        </div>
    </div>
</div>

@yield('contenido')

<div class="uk-container">

    <div class="uk-background-cover uk-background-muted uk-panel Footer" style="background-image: url(img/fondo_footer.png);">
        <div class="uk-container ContenedorArticulos" uk-scrollspy="cls: uk-animation-fade; delay: 500">
            <div class="uk-grid uk-grid-small ContenedorArticulos">
                <div class="uk-width-1-4@s">
                    <img src="{{asset('img/logo_footer.png')}}" class="ImgLogoFooter">
                </div>
                <div class="uk-width-1-4@s">
                    <h5>estilo kiut</h5>
                    <h4>catalogo</h4>
                    <ul>
                        <li><a>Kiut Cuadernos</a></li>
                        <li><a>Kiut Forever</a></li>
                        <li><a>Kiut Archivo</a></li>
                        <li><a>Arte</a></li>
                        <li><a>Backpacks</a></li>
                    </ul>
                    <img src="{{asset('img/barra_footer.png')}}" class="BarraFooter">
                </div>
                <div class="uk-width-1-4@s">
                    <div class="EnlaceFooter">
                        <a>dónde comprar</a>
                    </div>
                    <h4>contacto</h4>
                    <p>Siguenos en:</p>
                    <a href="" class="uk-icon-button uk-margin-small-right" uk-icon="icon: instagram; ratio: .8"></a>
                    <a href="" class="uk-icon-button  uk-margin-small-right" uk-icon="icon: facebook; ratio: .8"></a>
                    <a href="" class="uk-icon-button" uk-icon="icon: youtube; ratio: .8"></a>
                    <img src="{{asset('img/barra_footer.png')}}" class="BarraFooter BarraFooterAjuste">
                </div>
                <div class="uk-width-1-4@s">
                    <img src="{{asset('img/img_jean.png')}}" class="ImgJean">
                </div>
            </div>
        </div>
    </div>

    <div class="Terminos">
        <p>Todos los derechos reservados KIUT® 2019  -  Aviso de Privacidad</p>
    </div>

</div>

<script>
    $("body").css("overflow","hidden");
    $( document ).ready(function() {
        setTimeout(function(){
            $("body").css("overflow","visible");
            $('#status').fadeOut();
            $('#preloader').delay(150).fadeOut('slow');
        }, 300);
    });

</script>
</body>
</html>