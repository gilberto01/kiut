@extends('plantilla')

@section('contenido')
<div class="uk-container FondoBlanco">
    <div class="uk-grid">
        <div class="uk-width-2-5">
            <img src="{{asset('img/preloader-image.png')}}" class="Img_1">
            <img src="{{asset('img/img_2.png')}}" class="Img_2">
        </div>
        <div class="uk-width-expand">
            <img src="{{asset('img/norma-triangulo-full.png')}}" class="Img_3">
        </div>
    </div>
    <div class="uk-grid ContenidoCuadroBlog">
        <div class="uk-width-auto">
        </div>
        <div class="uk-width-expand">
            <div class="CuadroMorado"></div>
        </div>
    </div>
</div>

<div class="uk-container AlturaContenido">
    <div class="uk-flex uk-flex-center uk-flex-middle">
        <div class="uk-grid uk-grid-small ContenedorArticulos">
            <div class="uk-width-2-3@m">
                <div class="uk-grid">
                    <div class="uk-width-4-5@m">
                        <h2 uk-scrollspy="cls: uk-animation-slide-left; repeat: true" class="uk-heading-line uk-text-right TituloCuadro"><span>inspírate con kiut</span></h2>
                    </div>
                </div>
                <div uk-scrollspy="cls: uk-animation-slide-bottom; target: .uk-card; delay: 300; repeat: true">
                    @foreach($posts->slice(0, 1) as $imprimir)
                        <div class="uk-card uk-card-default CuadroNoticia CuadroGrande">
                            <div class="uk-card-body">
                                <div style="padding-left: 16px;">
                                    <h1>{!! $imprimir->titulo !!}</h1>
                                </div>
                                <div class="uk-grid uk-grid-collapse uk-flex uk-flex-right">
                                    <div class="uk-width-2-5@m">
                                        <h2 class="uk-heading-line uk-text-right"><span>{{$imprimir->categorias->nombre}}</span></h2>
                                    </div>
                                </div>
                                <p>{{$imprimir->descripcion}}</p>
                                <div class="EnlaceVerMas">
                                    <a>leer más</a>
                                </div>
                                <hr>
                                <div class="uk-text-right uk-margin-small-top">
                                    <i class="fas fa-comments"></i> 3
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="uk-background-cover uk-background-muted uk-panel uk-flex uk-flex-center uk-flex-middle AlturaFondoMorado" style="background-image: url(img/fondo_rosa.png);">
        <div class="uk-grid uk-grid-small ContenedorArticulos" uk-scrollspy="cls: uk-animation-fade; target: .uk-card; delay: 350; repeat: true">
            @foreach($posts->slice(1, 4) as $imprimir)
            <div class="uk-width-1-3@s">
                <div class="uk-card uk-card-default CuadroNoticia">
                    <div class="uk-card-media-top">
                        <img src="{{asset('img/'.$imprimir->imagen)}}" alt="">
                    </div>
                    <div class="uk-card-body">
                        <div class="uk-grid uk-flex uk-flex-right">
                            <div class="uk-width-3-4@m">
                                <h2 class="uk-heading-line uk-text-right"><span>{{$imprimir->categorias->nombre}}</span></h2>
                            </div>
                        </div>
                        <h3 class="uk-card-title">{{$imprimir->titulo}}</h3>
                        <p>{{$imprimir->descripcion}}</p>
                        <div class="EnlaceVerMas">
                            <a>leer más</a>
                        </div>
                        <div class="uk-text-right uk-margin-small-top">
                            <i class="fas fa-comments"></i> <small>4</small>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

<div class="uk-container FondoBlanco AlturaFondoBlanco">
    <div class="uk-text-center">
        <img src="{{asset('img/logo_fondo_blanco.jpg')}}" class="LogoEnBlanco">
    </div>
    <div class="uk-grid">
        <div class="uk-width-1-2" uk-scrollspy="cls: uk-animation-slide-bottom; target: img; delay: 300; repeat: true">
            <img src="{{asset('img/lapiz.png')}}" class="Img_fondo_blanco_1">
            <img src="{{asset('img/norma-libreta-happy.png')}}" class="Img_fondo_blanco_2">
            <img src="{{asset('img/img_4.jpg')}}" class="Img_fondo_blanco_3">
            <img src="{{asset('img/norma-libreta-the-wong.png')}}" class="Img_fondo_blanco_4">
        </div>
        <div class="uk-width-1-2 uk-text-right" uk-scrollspy="cls: uk-animation-slide-bottom; target: img; delay: 300; repeat: true">
            <img src="{{asset('img/hoja-dos.png')}}" class="Img_fondo_blanco_5">
            <img src="{{asset('img/img_3.png')}}" class="Img_fondo_blanco_6">
        </div>
    </div>
</div>

<div class="uk-container ContenedorArticulos CuadroCatalogo">
    <div class="uk-grid uk-grid-collapse uk-flex uk-flex-right">
        <div class="uk-width-1-3 ContenedorCatalogo">
            <h1>catálogo</h1>
            <h3>Crea tu mundo, hazlo más Kiut</h3>
            <p>Tenemos estilos tan variados como la personalidad de nuestras chicas Kiut. Kiut es un <strong>universo de creatividad, funcionalidad y diseño</strong> para las chicas de hoy.</p>
            <div>
                <a>conoce los productos</a>
            </div>
        </div>
    </div>
</div>

<div class="uk-container uk-text-right">
    <img src="{{asset('img/norma-mochila.png')}}" class="Img_fondo_blanco_7" uk-scrollspy="cls: uk-animation-slide-right; delay: 300; repeat: true">
</div>

@endsection