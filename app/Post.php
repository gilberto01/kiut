<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Post extends Model
{
    protected $table = 'posts';
    protected $fillable = [
        'titulo',
        'descripcion',
        'imagen',
        'categoria_id',
        'user_id'
    ];

    protected static function boot()
    {
        parent::boot();
        // Ordenar todo en DESC
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('id', 'desc');
        });
    }

    public function categorias()
    {
        return $this->hasOne('App\Categoria',"id","categoria_id");
    }

    public function autor()
    {
        return $this->hasOne('App\User',"id","user_id");
    }
}
