<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Categoria extends Model
{
    protected $table = 'categorias';
    protected $fillable = [
        'nombre',
    ];

    protected static function boot()
    {
        parent::boot();
        // Ordenar todo en DESC
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('id', 'desc');
        });
    }

    public function posts()
    {
        return $this->belongsTo('App\Post', "categoria_id");
    }
}
